---
title: Anna palautetta
---

[Google Forms](https://docs.google.com/forms/d/e/1FAIpQLSfTAfsdAzSOLr_W7PbYMeD3XtsmN3FY0EWYrPQfBuBX07hW-Q/viewform?usp=sf_link) (ei vaadi kirjautumista)

Voit myös lähettää palautetta, kysymyksiä tai muita kommentteja sähköpostilla osoitteeseen
palaute@sipu.li.

Palaute auttaa parantamaan sivustoa ja ohjeita :)
