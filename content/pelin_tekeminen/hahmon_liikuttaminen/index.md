---
title: Hahmon liikuttaminen
date: 2022-05-22
previous: fps_ja_piirtaminen
next: sisaltoa_peliin
---

Meillä on nyt hahmo, joka näkyy ikkunassa. Hahmo pitäisi saada liikkumaan silloin kun painetaan
nuolinäppäimiä. Eli jos nuolinäppäin on pohjassa, niin hahmon x-koordinaattia pitää muuttaa.
Tehdään while-silmukan sisään koodi, joka tarkistaa, onko nuolinäppäin pohjassa, ja liikuttaa
hahmoa jos on.

Pygamessa on funktio `pygame.key.get_pressed()`, joka palauttaa listan totuusarvoista, jotka 
kertovat onko jokin näppäin pohjassa vai ei. Pythonissa listan tietyn alkion saa lisäämällä 
listan sisältävän muuttujan perään `[]`-hakasulkeet, ja niiden sisälle sen alkion 
järjestysnumeron, jonka haluat hakea listasta. Pygamessa on määritetty näppäinten nimiä, jotka 
vastaavat järjestysnumeroita listassa. Tässä ovat nuolinäppäinten nimet pygamessa:

```
pygame.K_LEFT
pygame.K_RIGHT
pygame.K_UP
pygame.K_DOWN
```

Listan kaikista nimistä löydät [täältä](https://www.pygame.org/docs/ref/key.html#key-constants-label).

Tallennetaan näppäinlista muuttujaan. Tämä täytyy tehdä while-silmukan sisällä, koska muuten
muuttujaan tallentuu vain näppäinten alkutila eivätkä uudet näppäinpainallukset päivity 
listaan.

```python
pressed_keys = pygame.key.get_pressed()
```

Sitten tarkistetaan, onko vasen tai oikea nuolinäppäin painettu. Jos vasen on painettu, niin 
meidän täytyy vähentää leppiksen x-koordinaatista jokin sopiva määrä pikseleitä, niin että se
liikkuu vasemmalle. Muista, että x-koordinaatti on vasemmassa reunassa 0, ja oikeassa reunassa
ikkunan leveys. Jos oikea näppäin on painettu, niin meidän täytyy lisätä leppiksen 
x-koordinaattiin jokin määrä. Tallennetaan tämä määrä muuttujaan `leppis_speed` eli leppiksen
nopeus.

```python
# ...

leppis_image = pygame.image.load("leppis.png")
leppis_x = 350
leppis_y = 500
leppis_speed = 4

# ...

while running:
    # ...

    pressed_keys = pygame.key.get_pressed()

    if pressed_keys[pygame.K_LEFT]:            
        leppis_x = leppis_x - leppis_speed
    if pressed_keys[pygame.K_RIGHT]:
        leppis_x = leppis_x + leppis_speed

    # ...
```

Tässä siis tarkistetaan, onko pressed_keys-listan alkio pygame.K_LEFT (vasen nuolinäppäin) True
vai False, ja jos se on True, niin vähennetään leppiksen x-koordinaattia nopeuden verran. Sama
oikealle näppäimelle.

{{% huom %}}

Huom: `if lista[alkio]:` tarkoittaa samaa kuin `if lista[alkio] == True:`. Voit lisätä
koodiisi `== True`-päätteen if-lauseisiin kun haluat testata onko jokin muuttuja tosi, mutta
sitä ei ole pakko laittaa. If-lause nimittäin vain katsoo sille annettua totuusarvoa eikä 
välitä laittaako siihen suoraan totuusarvon vai tuleeko totuusarvo `==`-vertailusta.

{{% /huom %}}

Nyt kun suoritamme koodin, pystymme liikuttamaan leppistä vasemmalle ja oikealle 
nuolinäppäimillä. Saatat huomata, että leppis voi liikkua ikkunan ulkopuolelle. Voimme estää 
tämän tarkistamalla, onko leppiksen x-koordinaatti sallituissa rajoissa. Jos leppis on menossa
vasemmalta ikkunan ulkopuolelle, sen koordinaatti on vähemmän kuin nolla. Mutta jos leppis on 
menossa oikealta ikkunan ulkopuolelle niin meidän täytyy laskea oikea rajakoordinaatti ikkunan
leveyden avulla.

Ikkunan leveys on tällä hetkellä kirjoitettu numeroina sille koodiriville, jossa luodaan 
ikkuna. Meidän kannattaa kuitenkin tallentaa se (ja ikkunan korkeus) muuttujaan, jotta sitä 
voidaan käyttää uudelleen koodissa (koodiin voi toki kirjoittaa leveyden monta kertaa numerona,
mutta jos leveyttä haluaa muuttaa, on helpompaa muuttaa se vain kerran siltä riviltä jossa 
muuttuja määritellään). Määritellään muuttujat ja vaihdetaan ikkunanluomiskoodi käyttämään 
niitä:

```python
window_width = 1000
window_height = 800
window = pygame.display.set_mode((window_width, window_height)) 
```

Sitten tehdään lisäykset liikutuskoodiin. Vasemmalle liikkumisen ehto on helppo, leppiksen
x-koordinaatti ei vain saa mennä alle nollan:

```python
if pressed_keys[pygame.K_LEFT] and leppis_x - leppis_speed > 0:
    leppis_x = leppis_x - leppis_speed
```

Huomaa, että if-lauseelle voi antaa monta ehtoa lisäämällä and-sanan ehtojen väliin. Molempien 
ehtojen täytyy olla tosia, jotta if-rakenteen sisältämä koodi suoritetaan. On olemassa myös
or-sana, joka tarkoittaa sitä, että vain toisen ehdoista tarvitsee olla tosi.

Oikealle liikkumisen ehto on vähän vaikeampi laskea. Jos ehto on se, että uusi koordinaatti ei
saa mennä yli ikkunan leveyden, niin leppis voi silti liikkua jonkin verran reunasta yli, koska
sen x-koordinaatti tarkoittaa sen vasemman reunan koordinaattia. Oikea raja on siis vähemmän 
kuin ikkunan reunan koordinaatti. Rajan saa laskettua vähentämällä ikkunan leveydestä leppiksen
kuvan leveyden.

Pygamessa kuvilla on funktio `get_width()`, jota käytetään näin: `leppis_kuva.get_width()`.
Funktio palauttaa kuvan leveyden. Lasketaan oikeanpuoleinen raja ja lisätään liikkumiskoodiin
oikea ehto.

```python
right_boundary = window_width - leppis_image.get_width()
if pressed_keys[pygame.K_RIGHT] and leppis_x + leppis_speed < right_boundary:
    leppis_x = leppis_x + leppis_speed
```

Nyt leppiksen x-koordinaatti pysyy rajojen sisällä.

Koko koodi näyttää tältä:

```python
import pygame

window_width = 1000
window_height = 800
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Leppispeli")

fps = 60
clock = pygame.time.Clock()

white = (255, 255, 255)

leppis_image = pygame.image.load("leppis.png")
leppis_x = 350
leppis_y = 500
leppis_speed = 4

running = True

while running:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    pressed_keys = pygame.key.get_pressed()

    if pressed_keys[pygame.K_LEFT] and leppis_x - leppis_speed > 0:
        leppis_x = leppis_x - leppis_speed 
    right_boundary = window_width - leppis_image.get_width()
    if pressed_keys[pygame.K_RIGHT] and leppis_x + leppis_speed < right_boundary:
        leppis_x = leppis_x + leppis_speed
    
    window.fill(white)
    window.blit(leppis_image, (leppis_x, leppis_y))
    pygame.display.update()
```

[Seuraava artikkeli: Sisältöä peliin](/pelin_tekeminen/sisaltoa_peliin)
