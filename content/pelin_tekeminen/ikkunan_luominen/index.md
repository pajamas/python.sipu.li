---
title: Ikkunan luominen
date: 2022-05-20
previous: pygamen_asennus_linuxilla
next: fps_ja_piirtaminen
---

Nyt kun Python ja Pygame on asennettu, voimme alkaa koodaamaan peliä. Kannattaa lukea ensin
[Pythonin perusteet -artikkeli](/pythonin_perusteet/).

Aloitetaan tyhjästä tiedostosta. Ensin täytyy sisällyttää Pygame-kirjasto koodiin, jotta sitä 
voi käyttää. Pythonissa on monia kirjastoja, joihin sisältyy erilaisia toimintoja. Mitään näitä 
ei pysty käyttämään koodissa automaattisesti, vaan täytyy kirjoittaa koodin alkuun komento, 
joka kertoo Pythonille, että tiettyä kirjastoa halutaan käyttää. Tämä tehdään näin:

```python
import pygame
```

Nyt Pygamen toimintoja pystyy käyttämään.

Ensimmäinen asia mitä haluamme tehdä on ikkunan luominen. Pygameen kuuluu funktio, joka luo
ikkunan, mutta se tarvitsee argumentiksi ikkunan leveyden ja korkeuden. Valitaan leveydeksi
1000 pikseliä ja korkeudeksi 800 pikseliä. Funktiota kutsutaan näin:

```python
pygame.display.set_mode((1000, 800))
```

Funktiokutsussa näkyy kahdet sulut, koska tämä funktio ottaa argumentiksi tuplen. Tuple on
yhdenlainen lista, ja se luodaan tähän tyyliin:

```python
mun_tuple = (asia1, asia2, asia3, asia4)
```

Ikkunanluomisfunktion tuple sisältää siis kaksi numeroa, leveyden ja korkeuden.

Voimme myös asettaa ikkunalle otsikon/nimen. Tämä tehdään tällä komennolla:

```python
pygame.display.set_caption("Leppispeli")
```

Voit laittaa otsikoksi mitä vain haluat.

Tässä vaiheessa jos koodin yrittää suorittaa, niin ikkuna ilmestyy alle sekunniksi ja sulkeutuu
sitten. Tämä johtuu siitä, että koodissa ei ole mitään muuta sisältöä kun ikkunan luominen. Kun
ikkuna on luotu, niin koodin suoritus pääsee tiedoston loppuun, ja ohjelma sulkeutuu. Jos 
haluamme pitää ikkunan auki, niin meidän täytyy kirjoittaa silmukka. Silmukka on koodipätkä,
joka saa ohjelman toistamaan samoja koodirivejä niin kauan kunnes jokin ehto ei enää täyty. 

Ensin luodaan muuttuja, joka toimii ehtona. Muuttujan nimeksi voi laittaa vaikka `running`, tai
mitä vain haluat. Tämä muuttuja tulee siis merkitsemään sitä, onko peli käynnissä vai pitääkö
ohjelman sulkeutua. Pythonissa on joitakin eri tietotyyppejä, joita muuttujiin voi tallentaa.
Yksi näistä tyypeistä on totuusarvo. Totuusarvomuuttuja voi saada arvon `True` tai `False`, eli
totta tai ei-totta. Annetaan muuttujalle arvo `True`, koska peli on aluksi käynnissä. Muuttuja
luodaan näin:

```python
running = True
```

Nyt arvo `True` eli totta on tallennettu running-nimiseen muuttujaan. Jos koodissa jossain 
kohtaa lukee running, niin Python sijoittaa siihen kohtaan running-muuttujaan tallennetun 
arvon. Poikkeus tähän on se, jos running lukee `=`-merkin vasemmalla puolella, sillä tämä
tarkoittaa sitä että running-muuttujalle annetaan toinen arvo.

### Pelisilmukka

Silmukka toimii niin, että sille annetaan jokin ehto, esimerkiksi muuttuja, ja se tarkistaa 
onko ehto totta. Jos on, niin silmukka suorittaa sisällään olevat koodirivit. Jos ehto ei ole
totta, niin silmukan suoritus loppuu ja ohjelma siirtyy silmukan jälkeisten koodirivien
suorittamiseen.

Tässä on esimerkki silmukasta:

```python
running = True

while running:
    print("Peli käynnissä")
```

Tämä silmukka tulostaa tekstiä "Peli käynnissä" kunnes ohjelman sulkee. Tämä silmukka ei ikinä
lopu jos ohjelmaa ei sulje itse (painamalla Ctrl + C). Silmukasta kannattaa siis tehdä 
sellainen, että sen suorittamisehto ei enää täyty jossain kohtaa, jotta se loppuu joskus. 
Tehdään silmukasta kohta tällainen.

Silmukan ja monien muiden Python-koodirakenteiden kirjoittamisessa kannattaa huomioida 
oikeanlaiset sisennykset. Silmukan sisällä olevat koodirivit pitää sisentää painamalla tabia,
tai muuten ne eivät kuulu silmukkaan. Jos silmukan sisälle tekee jonkin toisen sisennettävän 
rakenteen, niin sen täytyy olla sisennetty kaksi kertaa tabia painamalla.

### Eventit eli tapahtumat

Jotta peli voi olla interaktiivinen, sen täytyy tunnistaa erilaisia tapahtumia, kuten käyttäjän
napinpainalluksia. Tärkeä tapahtuma on ikkunan sulkeminen. Koodataan tämä toiminto peliin.

```python
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
```

Tässä koodissa while-silmukan sisään on lisätty toisenlainen silmukka eli for-silmukka. 
For-silmukalle kuuluu antaa jonkinlainen lista, jonka jokaiselle sisältämälle asialle se 
suorittaa jotain koodia. Silmukka myös tallentaa jokaisen listan sisältämän asian nimeämääsi
muuttujaan, eli tässä tapauksessa muuttujaan `event`. Tässäkin kohtaa muuttujalle voi antaa
haluamansa nimen, eli sen ei ole pakko olla event. Pygame pitää kirjaa uusista tapahtumista
ja niihin pääsee käsiksi kutsumalla Pygamen funktiota `pygame.event.get()`.

Silmukan sisällä tarkastellaan event-muuttujaa if-rakenteessa. If-rakenne toimii niin, että
sille annetaan ehto, ja jos ehto on tosi, niin sen sisältämä koodi suoritetaan (vain kerran).
Katsotaan mitä tässä if-rakenteessa tapahtuu:

```python
if event.type == pygame.QUIT:
    running = False
```

Pygame-kirjastossa on määritetty niin, että tapahtumilla on jokin tyyppi eli `type`. Lisäksi
`pygame.QUIT` on määritetty tarkoittamaan sitä, että käyttäjä sulkee ikkunan. Joten kun 
for-silmukassa käydään läpi tapahtumalistaa, niin jos listasta löytyy tapahtuma jonka tyyppi on
`pygame.QUIT`, niin running-muuttujan arvo muutetaan epätodeksi. Huomaa, että jos halutaan
vaihtaa muuttujan arvoa, niin käytetään vain yhtä yhtäsuuruusmerkkiä, mutta jos halutaan 
vertailla kahta asiaa (ovatko ne yhtä suuret) niin täytyy käyttää kahta peräkkäin.

Koodin voi siis selittää näin:

```
toistetaan sillä aikaa kun running-muuttuja sisältää arvon True:
    käydään läpi pygame.event.get()-funktion antamat arvot ja läpikäydessä 
    kutsutaan kutakin arvoa nimellä event:
        jos event-muuttujan sisältämän asian tyyppi on pygame.QUIT:
            muutetaan running-muuttujan arvo epätodeksi
``` 

Nyt ollaan saatu aikaiseksi se, että avataan ikkuna (jossa ei vielä näy mitään) ja ikkuna
pysyy auki kunnes käyttäjä sulkee sen, jolloin ohjelman suoritus päättyy. Koko koodi näyttää
tältä:

```python
import pygame

pygame.display.set_mode((1000, 800))                 
pygame.display.set_caption("Leppispeli")

running = True    

while running:    
    for event in pygame.event.get():    
        if event.type == pygame.QUIT:    
            running = False
```

[Seuraava artikkeli: FPS ja piirtäminen](/pelin_tekeminen/fps_ja_piirtaminen)
