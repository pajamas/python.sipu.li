---
title: Tietoa sivusta
---

Aloitettu vuonna 2022

Voit lähettää palautetta [Google Formsilla](https://docs.google.com/forms/d/e/1FAIpQLSfTAfsdAzSOLr_W7PbYMeD3XtsmN3FY0EWYrPQfBuBX07hW-Q/viewform?usp=sf_link) 
tai sähköpostilla osoitteeseen palaute@sipu.li tai anna@sipu.li.
