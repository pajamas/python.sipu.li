---
title: Pythonin perusteet
date: 2022-01-01
---

Python-ohjelma on vähän kuin lista ohjeista, jotka tietokone suorittaa (eli tekee mitä ohjeessa 
käsketään). Suoritus alkaa ensimmäiseltä riviltä ja loppuu viimeiseen riviin. Ohjelman suoritus
saattaa myös keskeytyä ennenaikaisesti, jos suorituksessa tulee jokin virhe.

### Tulostaminen

Tulostaminen tarkoittaa sitä, että ohjelma tuottaa näkyville jotain tekstiä. Ilman
tulostamista ohjelma ei mitenkään kommunikoi käyttäjälle mitä tapahtuu. Tulostaminen 
auttaa koodaajaa seuraamaan koodin suoritusta, ja kertoo käyttäjälle ohjelman lopputuloksen.

Pythonissa tulostetaan tekstiä `print`-komennolla.

```python
print("Hei!")
print("Toinen print-komento")
```

```tulostus
Hei!
Toinen print-komento
```

Tekstiä eli merkkijonoja tulostaessa täytyy muistaa heittomerkit tai muuten koodi ei toimi.

Print-komennolla voi tulostaa myös muuta dataa kuin merkkijonoja, esimerkiksi numeroita tai
listoja.

### Laskutoimitukset

Pythonissa laskutoimituksia tehdään näillä merkeillä:

- \+ (yhteenlasku)
- \- (vähennyslasku)
- \* (kertolasku)
- / (jakolasku)
- % (jakojäännös)
- \*\* (potenssiin korotus)

```python
print(4+6)
```

```tulostus
10
```

### Muuttujat

Usein ohjelmissa käytetään monissa kohdissa samaa arvoa. Tällainen arvo kannattaa tallentaa
muuttujaan, niin että jos sitä muuttaa, niin ei tarvitse kirjoittaa uudelleen montaa kohtaa 
koodista. Toinen tilanne, jossa täytyy käyttää muuttujaa, on tilanne, jossa saman koodipätkän
halutaan toimivan monella eri arvolla. Esimerkiksi koodi, joka laskee yhteen kaksi arvoa.
Arvoja ei pysty kirjoittamaan numeroina, koska niitä ei tiedetä etukäteen, joten niiden 
paikalle laitetaan kaksi muuttujaa.

Muuttuja määritellään näin:

```python
numero = 10
```

Nyt arvo 10 on tallennettu `numero`-nimiseen muuttujaan, ja voit käyttää tätä muuttujaa missä
vain mihin voisit laittaa luvun 10:

```python
numero = 10
print(numero + 4)
```

```tulostus
14
```

Muuttujan arvoa muutetaan näin:

```python
# muuttuja luodaan
x = 100
# muuttujan arvoa muutetaan
x = 2
```

Muuttujan arvoa voidaan myös muuttaa laskutoimituksilla. Tähän on kaksi tapaa:

```python
x = 100
# lisätään x:ään 40 kahdella tavalla:

x = x + 40
# eli muutetaan arvoksi 100 + 40 eli 140 (koska alunperin x:n arvo oli 100)

x += 40
# tämä tarkoittaa samaa kuin ylempi tapa
```

Voi käyttää kumpaa vain tapaa. Esimerkissä käytetään yhteenlaskua, mutta tämä toimii myös muilla
laskutoimituksilla. Huomaa, että jos x-muuttujaa ei ole määritelty ennen lisäämistä, niin koodi
ei toimi. Voit kokeilla tätä ottamalla ylimmän rivin `x = 100` pois. Huomaa myös, että tässä
tilassa koodi ei tulosta mitään. Voit lisätä koodin loppuun rivin `print(x)`, jos haluat tietää
x:n loppuarvon.

Muuttujat kannattaa nimetä niin, että nimi kuvaa muuttujan sisältöä. Eli esimerkiksi
`valonnopeus` olisi hyvä muuttujan nimi, mutta `asdfgh` ei niinkään.

Muuttujaan voi tallentaa eri tyyppisiä arvoja:

- Kokonaisluku: `124`
- Liukuluku: `5.27`
- Totuusarvo: `True` tai `False`
- Merkkijono: `"sipuli"`
- Lista: `[1, 2, 4, 8, 16]`

Myös muita datatyyppejä on olemassa.

### Kommentit

Kommentit ovat tekstiä, jota Python ei tulkitse koodiksi. Kommenteilla voi selventää koodia ja
kertoa mitä koodi tekee. Pythonissa kommentti alkaa `#`-merkillä:

```python
# tämä on kommentti
```

Monen rivin kommentteja voi tehdä muodostamalla monirivisen merkkijonon, jota ei käsitellä
mitenkään. Monirivinen merkkijono tehdään kolmella lainausmerkillä:

```python
"""tämä
on
merkkijono
jolla ei tehdä mitään
eli vähän niin kuin kommentti
;)
"""
```

### Tehtävät

Tehtävät toimivat esimerkkeinä siitä, miten artikkelissa opitut asiat toimivat käytännössä.
Kirjoita koodipätkä, joka täyttää tehtävän vaatimukset, ja sitten testaa, toimiiko se (ajamalla
koodi). Esimerkkiratkaisu löytyy tehtävän alta, mutta tehtävän voi ratkaista monella tavalla. 
Voit käyttää artikkelia apuna. Myös Googlea kannattaa käyttää aktiivisesti. Liian 
helppoja tehtäviä ei tarvitse tehdä. 

{{% exercise num="1" %}}
Kirjoita ohjelma, joka tulostaa kolme eri merkkijonoa.
{{< solution >}}
```python
print("Jono 1")
print("Jono 2")
print("Jono 3")
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="2" %}}
Kirjoita ohjelma, jossa ensin tallennetaan muuttujaan merkkijono, ja sitten tämä muuttuja
tulostetaan.
{{< solution >}}
```python
jono = "Moikka"
print(jono)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="3" %}}
Kirjoita ohjelma, jossa muuttujaan tallennetaan luku 0, se tulostetaan, siihen lisätään 10,
se tulostetaan, ja se jaetaan kahdella ja tulostetaan vielä kerran.
{{< solution >}}
```python
x = 0
print(x)
x += 10
print(x)
x /= 2
print(x)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="4" %}}
Kirjoita ohjelma, jossa muuttujaan tallennetaan luku 5, ja ohjelma tulostaa luvun 7 niin, 
että sen laskemiseen käytetään muuttujaa, mutta muuttujan arvo ei muutu.
{{< solution >}}
```python
numero = 5
print(numero + 2)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="5" %}}
Kirjoita ohjelma, jossa ensin tallennetaan luku 10 muuttujaan, ja sitten tehdään toinen 
muuttuja, johon tallennetaan ensimmäisen muuttujan arvo kerrottuna kahdella, ja molemmat 
muuttujat tulostetaan.
{{< solution >}}
```python
x = 10
y = x*2
print(x)
print(y)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="6" %}}
Tallenna johonkin muuttujaan joko arvo `True` tai `False`. Anna muuttujalle nimi, joka kuvaa
jotakin asiaa, jota voisi kuvailla totuusarvon avulla. Siis jokin asia, joka on tosi tai
epätosi, tai kysymys, johon voi vastata kyllä tai ei.
{{< solution >}}
```python
hahmoElossa = True
```
{{% /exercise %}}
{{< /solution >}}
