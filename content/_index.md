---
title: Etusivu
frontpage: true
---

Opetellaan koodaamaan Python-ohjelmointikielellä!

{{% frontpagelinks %}}

{{% frontpagelink url="artikkelit" %}}
### Artikkelit

Ohjeita ja tehtäviä.
{{% /frontpagelink %}}

{{% frontpagelink url="/pelin_tekeminen" img="/pelin_tekeminen/fps_ja_piirtaminen/leppis.png" %}}

### Pelin tekeminen

Tehdään peli Pygamella.

{{% /frontpagelink %}}

{{% frontpagelink url="/palaute" %}}

### Anna palautetta

Auta parantamaan sivustoa lähettämällä palautetta.

{{% /frontpagelink %}}


{{% frontpagelink class="" url="/asetukset/" %}}

### Asetukset

Vaihda kursoria tai teemaa.

{{% /frontpagelink %}}

{{% /frontpagelinks %}}
