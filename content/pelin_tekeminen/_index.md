---
title: Pelin tekeminen
---

Tehdään peli Pythonilla. Ennen tutoriaalin seuraamista on hyvä tietää nämä asiat:

- Tärkeintä on saada Python ja pygame toimimaan. Jos Pythonin asennuksessa tulee ongelmia vastaan,
niin Googlesta voi löytyä apua. Python on suosittu ohjelmointikieli ja sitä varten on paljon
resursseja ja tutoriaaleja netissä. Kannattaa etsiä toisia ohjeita asennukseen, jos tämän sivuston
ohjeet eivät toimi. Ongelmatilanteiden sattuessa voit myös [lähettää palautetta](/palaute/) ja
kertoa ongelmasta, jos haluat osallistua sivun ja ohjeiden kehittämiseen.
- Koodatessa on hyvä ymmärtää englantia, koska suurin osa koodausresursseista on englanniksi, ja
koodikielten sanat ovat englanniksi.
- Kokeilemalla oppii. Kannattaa koittaa muokata koodin eri osia ja katsoa, miten muutokset 
vaikuttavat ohjelmaan.
- Koodatessa tulee paljon virheviestejä vastaan, ja se on ihan ok: koodaaminen on prosessi, joka
vaatii paljon vaiheittaisia parannuksia ja virheiden korjaamista. Kukaan ei saa kokonaista ohjelmaa 
valmiiksi kerralla, vaan muutaman rivin kirjoittamisen jälkeen kannattaa testata, toimiiko koodi,
ja jos ei toimi, niin muokata koodia niin että se alkaa toimimaan. Jos et tiedä mitä virheviesti
tarkoittaa, niin voit kopioida sen Googleen ja katsoa, onko muilla ollut samanlaista ongelmaa.
Voit myös etsiä virheviestiä 
[virheviestien tulkitseminen -artikkelista](/artikkelit/virheviestien_tulkitseminen) (kaikkia ei 
kuitenkaan löydy artikkelista).
- Jos haluat tietää lisää jostain koodikonseptista, kannattaa lukea tämän sivuston 
[artikkeleja](/artikkelit), tai etsiä tietoa Googlesta tai vaikka Youtubesta.
- Jokainen uusi opittu asia ja jokainen onnistunut koodipätkä on ylpeyden aihe!

Sitten voit aloittaa pelin tekemisen:
