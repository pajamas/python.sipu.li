---
title: Pythonin ja Pygamen asennus Windowsilla
date: 2022-05-18
next: pygamen_asennus_linuxilla
---

{{% huom %}}

Muutos ohjeisiin (27.10.22): Pythonin uusimmalla versiolla 3.11 ei vielä pysty asentamaan Pygamea.
Lataa siis tältä sivulta https://www.python.org/downloads/windows/ esimerkiksi versio 3.10.8 
painamalla oikean version alta linkkiä `Download Windows installer (64-bit)`.

{{% /huom %}}

Jos sinulla on Windows-kone, niin mene osoitteeseen 
https://www.python.org/downloads/ ja paina nappia, jossa lukee "Download Python" (ja 
version numero). Lataa tiedosto koneellesi ja avaa se. Tee mitä asennusohjelmassa käsketään.

Jotta voimme testata, onnistuiko Pythonin asentaminen, täytyy avata Windowsin komentokehote. 
Komentokehote on ohjelma, jolla voi käsitellä tiedostoja ja suorittaa muita ohjelmia. Hae ohjelmaa 
'komentokehote' ja avaa se. 

![Kuva komentokehotteesta haussa](komentokehote.png)


Kirjoita kehotteeseen `py` ja paina enteriä. Jos komentokehotteeseen ilmestyy tämän näköinen teksti, niin Pythonin asentaminen onnistui.                      

```
Python 3.9.13 (tags/v3.9.13:6de2ca5, May 17 2022, 16:36:42) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Jos tapahtuu jotain muuta, niin yritä uudelleen asennusta.

### Pygamen asentaminen

Sulje Python kirjoittamalla komentokehotteeseen `exit()`. Nyt meidän täytyy vielä asentaa 
Pygame-kirjasto Pythoniin, jotta pelin tekeminen onnistuu. Kirjoita komentokehotteeseen tämä 
komento:

```
py -m pip install pygame
```

Kun asennus on valmis, voit sulkea komentokehotteen.

{{% huom %}}

Huom: jos komentokehote jää jumiin ja asennuspalkki näyttää olevan täynnä mutta mitään ei 
tapahdu, niin paina enteriä (tämän pitäisi auttaa). Jos kehotteeseen ilmestyy teksti 
"Successfully installed pygame", niin asennus onnistui. 

{{% /huom %}}

Avaa Tiedostot ja tee jonnekin kansio, johon teet pelisi. Sen jälkeen avaa tekstieditori, jolla
haluat koodata. Pythonin mukana tulee IDLE-niminen tekstieditori, joka sopii hyvin tähän 
tarkoitukseen. IDLEssä tee File-valikosta uusi tiedosto, ja kirjoita sinne `print("Hei")`. 
Tallenna tiedosto tekemääsi kansioon ja sitten paina Run-valikosta Run Module. Sinulle pitäisi 
avautua uusi ikkuna, jossa lukee muutama rivi 
tekstiä ja sitten `Hei`. Voit pitää tämän ikkunan auki ja kokeilla painaa uudestaan Run Module. 
Koodissa tulostettu teksti ja mahdolliset virheet tulevat tähän ikkunaan (lisäksi jotkin virheet
voivat näkyä pienessä virheikkunassa).

Lisää kooditiedoston alkuun tämä rivi:

```python
import pygame
```

Jos nyt painat Run Module-napista, niin koodin pitäisi tulostaa nämä rivit:

```
pygame 2.1.2 (SDL 2.0.18, Python 3.9.13)
Hello from the pygame community. https://www.pygame.org/contribute.html 
Hei
```

Jos tämä tulostui, niin Pygame toimii.

### Visual Studio Coden asentaminen

IDLEn sijaan voit myös käyttää jotain muuta koodieditoria. Yksi tällainen on Visual Studio Code
(VS Code). Sen voi ladata tältä sivulta: https://code.visualstudio.com/

Kun olet asentanut VS Coden, avaa se. Vasemmasta palkista etsi ikoni, jonka nimi on 
'Extensions'. Tähän näkymään pääsee myös painamalla `Ctrl + Shift + X`. Hae lisäosaa Python, ja
asenna se. Sitten mene File-valikkoon (ylhäällä vasemmalla), ja valitse Open Folder. Etsi 
kansio, jonka olet luonut peliä varten ja avaa se. Avaa tiedosto, joka tehtiin viime vaiheessa.
Sitten paina oikeasta yläkulmasta oikealle osoittavaa nuolta. 

![VS Coden koodinsuorittamisnappi](run-button.png)

Tämän pitäisi suorittaa 
Python-koodi, joka tiedostoon on kirjoitettu. Koodin tulostaman tekstin pitäisi näkyä ikkunan
alareunassa. Jos teksti näkyy, niin olet valmis käyttämään VS Codea pelin koodaamiseen.

[Seuraava artikkeli: Ikkunan luominen](/pelin_tekeminen/ikkunan_luominen)
