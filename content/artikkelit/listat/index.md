---
title: Listat
date: 2022-01-04
---

Lista on datatyyppi, joka sisältää monta arvoa. Arvot voivat olla mitä vaan datatyyppiä, myös
listoja. Yksittäistä listassa olevaa arvoa sanotaan alkioksi.

Lista määritellään näin:

```python
# lista joka sisältää valmiiksi neljä alkiota
lista1 = ["abc", "def", "ghi", "jkl"]
# tyhjä lista
lista2 = []
```

Listassa olevaan alkioon viitataan sen järjestysluvun avulla, ja nämä järjestysluvut alkavat
nollasta. Esimerkiksi listan ensimmäinen alkio tulostetaan näin:

```python
lista = [1,2,3,4,5]
print(lista[0])
```

```tulostus
1
```

Alkion järjestysnumero siis laitetaan hakasulkuihin listan nimen perään.

### Alkioiden muokkaaminen, lisääminen ja poistaminen

Listan alkioita muokataan järjestysnumeron perusteella:

```python
lista = [1,2,9,4,5]
lista[2] = 3
print(lista)
```

```tulostus
[1, 2, 3, 4, 5]
```

Listaan lisätään uusia alkioita `append`-metodilla.

```python
lista = []
lista.append(100)
lista.append(200)
lista.append(300)
print(lista)
```

```tulostus
[100, 200, 300]
```

{{% huom %}}

Huom: Metodi tarkoittaa funktiota, joka kirjoitetaan jonkin muuttujan nimen perään, pisteen 
jälkeen. Tällainen funktio pystyy käsittelemään argumenttien lisäksi myös sitä muuttujaa, 
jonka perään se on kirjoitettu. Tietyillä datatyypeillä on omat metodinsa.

{{% /huom %}}

Listasta voi poistaa alkioita kahden metodin avulla: `remove` ja `pop`. 

Remove ottaa argumentiksi sen alkion, jonka haluat poistaa.

```python
lista = ["a", "b", "c"]
print(lista)
lista.remove("b")
print(lista)
```

```tulostus
['a', 'b', 'c']
['a', 'c']
```

Pop ottaa argumentiksi poistettavan alkion järjestysnumeron.

```python
lista = ["a", "b", "c", "d", "e"]
print(lista)
lista.pop(2)
print(lista)
```

```tulostus
['a', 'b', 'c', 'd', 'e']
['a', 'b', 'd', 'e']
```

### Muuta listoihin liittyvää

Listan pituuden voi selvittää `len`-funktiolla:

```python
lista = [1,2,3,4,5]
pituus = len(lista)
print(f"Listan pituus on {pituus}.")
```

```tulostus
Listan pituus on 5.
```

Jos haluat testata, onko listassa tietty alkio, tämä onnistuu `in`-avainsanalla:

```python
lista = [1,2,3,4,5]
if 2 in lista:
    print("Lista sisältää luvun 2.")
```

```tulostus
Lista sisältää luvun 2.
```

Tietyn alkion järjestysnumeron voi selvittää `index`-metodilla:

```python
lista = ["a","b","c","d","e"]
# tulosta c:n järjestysnumero listassa
print(lista.index("c"))
```

```tulostus
2
```

Index tarkoittaa siis järjestysnumeroa.

### Tuplet

Tuple on toinen listamainen datatyyppi, mutta se eroaa listasta siten, että tuplea ei voi
muuttaa sen luomisen jälkeen. Siihen ei voi lisätä alkioita, eikä alkioita voi poistaa, eikä
olemassaolevien alkioiden arvoja voi muuttaa. Tuple muodostetaan samalla tavalla kuin lista, 
mutta hakasulkujen sijaan siinä käytetään tavallisia sulkeita.

{{% huom %}}

Huom: Vaikka tuplea ei voi muokata, tuplen sisältävään muuttujaan voi kuitenkin tallentaa uuden
tuplen. Tämä tehdään samalla tavalla kuin uuden arvon tallentaminen mihin tahansa muuhunkin 
muuttujaan:

```python
mun_tuple = (1,2,3)
print(mun_tuple)
# tallennetaan muuttujaan uusi tuple
mun_tuple = (4,5,6)
print(mun_tuple)
# yritetään muokata tuplen toista alkiota
mun_tuple[1] = 8
```

```tulostus
(1, 2, 3)
(4, 5, 6)
Traceback (most recent call last):
  File "/home/anna/python/listajutut.py", line 5, in <module>
    mun_tuple[1] = 8
TypeError: 'tuple' object does not support item assignment
```

Tässä nähdään, että muuttujaan pystyi tallentamaan uuden tuplen, mutta tuplen sisältöä ei 
pystynyt muokkaamaan.

{{% /huom %}}

### Sanakirjat

Sanakirja on kolmas kokoelmadatatyyppi, mutta se eroaa listasta ja tuplesta siten, että sen 
alkioita ei pysty hakemaan järjestysnumerolla. Sen sijaan niitä haetaan avaimilla. Nämä avaimet
voivat olla mitä vain datatyyppiä, ja avain määritetään samalla kun sanakirjaan tallennetaan
arvo.

Sanakirjaa luodessa avaimet ja arvot erotellaan toisistaan kaksoispisteillä, ja sanakirjan
ympärille laitetaan aaltosulkeet.

```python
hinnat = { "omena": 0.5, "leipä": 1.5, "paita": 20 }
hinnat["leivänpaahdin"] = 30
print(hinnat["omena"])
print(hinnat["leipä"])
print(hinnat)
```

```tulostus
0.5
1.5
{'omena': 0.5, 'leipä': 1.5, 'paita': 20, 'leivänpaahdin': 30}
```
