---
title: Pygamen asennus Linuxilla
date: 2022-05-19
previous: pygamen_asennus_windowsilla
next: ikkunan_luominen
---

Pythonin pitäisi jo olla asennettuna koneellesi, jos käytät Ubuntua. Kannattaa testata, mikä komento
avaa Python-kehotteen. Se voi olla joko `python` tai `python3`. Varmista, että Pythonin versio alkaa 
3:lla, esimerkiksi Python 3.9.7. Se näkyy ensimmäisellä rivillä kun avaat Python-kehotteen. Jos 
`python`-komentoa käyttäessä versio alkaa 2:lla, niin käytä `python3`-komentoa. Python 2 on vanha 
versio. Eli jos python-komento ei toimi tai se käyttää vanhaa versiota, niin käytä python3-komentoa
aina kun ohjeissa lukee python.

Avaa terminaali ja suorita tämä komento (jos käytät Ubuntua):

```
sudo apt install python3-pygame
```

Jos asennus onnistuu, niin tee kansio, johon aiot pelisi tehdä. Tämän voi tehdä 
tiedostojenhallintaohjelmassa tai komentorivillä. Jos haluat tehdä kansion komentorivillä, niin se
onnistuu näin:

### Kansion tekeminen komentorivillä

Tarkista ensin, missä kansiossa olet nyt. Terminaaliohjelma avaa aina aluksi kotikansion. Olet 
kotikansiossa, jos komentosyötteesi näyttää tältä:

```
nimesi@koneesiNimi:~$
```

Merkki `~` tarkoittaa kotikansiota.

Tarkistetaan, mitä tiedostoja ja kansioita kotikansiostasi löytyy. Kirjoita terminaaliin `ls` ja paina
enteriä. Kansioiden nimet näkyvät lihavoituna ja tiedostot normaalina tekstinä. Sinulta pitäisi löytyä
kansio nimeltä Documents. Jos ei löydy, niin tee tämänniminen kansio komennolla `mkdir Documents`.
Sitten siirry Documents-kansioon komennolla `cd Documents`. Nyt komentosyötteesi pitäisi näyttää 
tältä:

```
nimesi@koneesiNimi:~/Documents$
```

Voit listata Documents-kansion sisällön komennolla `ls`, mutta ei ole pakko. Tehdään uusi kansio
pelille. Suorita tämä komento:

```
mkdir pygame-peli
```

Nyt Documentsista pitäisi löytyä pygame-peli -niminen kansio. Voit tehdä myös muunnimisen kansion.
Voit tehdä nyt myös Python-kooditiedoston tällä komennolla:

```
touch peli.py
```

### Ensimmäinen koodirivi

Nyt voit avata haluamasi tekstieditorin, esimerkiksi Kate toimii hyvin. Jos haluat hienomman 
koodieditorin ja käytät Ubuntua, niin voit ladata Visual Studio Coden tällä komennolla:

```
sudo apt install code
```

Avaa haluamasi tekstieditori. Jos olet jo tehnyt Python-tiedostoon tekemääsi kansioon, niin avaa
tiedosto. Muussa tapauksessa tallenna tähän kansioon uusi tiedosto, esimerkiksi `peli.py`.

Lisää tiedostoon tämä koodirivi:

```python
print("Hei")
```

Tallenna tiedosto. Sitten avaa terminaali ja navigoi siihen kansioon, jossa `peli.py` sijaitsee.
Eli esimerkiksi komennolla `cd ~/Documents/pygame-peli`. Muista, että kun olet kirjoittanut muutaman
merkin kansion nimeä, niin voit painaa Tab-näppäintä, joka lisää komentoon lopun kansion nimestä.
Näin ei siis tarvitse kirjoittaa kansion koko nimeä itse.

Kun olet siinä kansiossa, jossa kooditiedosto sijaitsee, niin suorita se tällä komennolla:

```
python peli.py
```

Tämän komennon pitäisi tulostaa sana Hei.

Mene takaisin tekstieditoriin ja lisää ennen print-komentoa tämä rivi:

```python
import pygame
```

Sitten tallenna tiedosto ja suorita se taas komentorivillä. Koodin pitäisi tulostaa nämä rivit:

```
pygame 1.9.6
Hello from the pygame community. https://www.pygame.org/contribute.html
Hei
```

Jos tulostus näyttää tältä niin pygame toimii.

[Seuraava artikkeli: Ikkunan luominen](/pelin_tekeminen/ikkunan_luominen)
