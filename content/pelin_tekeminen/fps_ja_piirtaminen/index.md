---
title: FPS ja piirtäminen
date: 2022-05-21
previous: ikkunan_luominen
next: hahmon_liikuttaminen
---

Ennen kuin aletaan piirtämään ikkunaan asioita, niin määritetään pelin FPS (frames per second
eli piirtämisnopeus). Jos sitä ei määritetä, niin pelin nopeus on niin suuri kuin vain
tietokoneessasi riittää tehoja, eli turhan iso. Lisätään koodiin pari riviä:

```python
# ...
fps = 60
clock = pygame.time.Clock()

while running:
    clock.tick(fps)
    # ...
```

Ensin siis määritellään muuttuja fps, joka sisältää sen kuinka monta kertaa sekunnissa haluamme 
while-silmukan pyörivän. Sitten luodaan pygamen Clock-objekti, jonka avulla fps:ää säädellään.
Sitten laitetaan while-silmukkaan rivi, joka säätelee fps:ää äsken luodun Clock-objektin 
avulla. Tick-funktio ottaa argumentiksi halutun fps:än. Se on tallennettuna `fps`-muuttujaan,
joten annetaan muuttuja funktiolle argumentiksi.

### Ikkunaan piirtäminen

Jotta ikkunaan voi piirtää, tarvitsemme muuttujan, joka kuvaa ikkunaa. Alussa kutsumamme 
`pygame.display.set_mode`-funktio palauttaa tällaisen ikkunaa kuvaavan objektin. Voimme 
tallentaa tämän objektin muuttujaan näin:

```python
window = pygame.display.set_mode((1000, 800))
```

Älä siis lisää toista funktiokutsua, vaan kirjoita samalle riville `window =`, jotta funktion
antama objekti tallentuu window-muuttujaan.

Sitten voimme piirtää ikkunaan. Täytetään ikkuna ensin vaikka valkoisella värillä. Pygamessa
värejä kuvataan tuplella, joka sisältää kolme numeroa, joista ensimmäinen tarkoittaa punaisen
määrää, toinen vihreän määrää ja kolmas sinisen määrää: siis RGB-arvona. Jos etsit netistä 
"color picker", voit valita itse jonkun värin. Katso vain mitkä sen R, G ja B -arvot ovat.
Esimerkiksi valkoinen väri on RGB-arvoina `(255,255,255)` ja musta on `(0,0,0)`.

Määritellään koodin alkuun muuttuja, johon sijoitamme valkoisen (tai muun haluamasi) värin 
RGB-arvon. Muuttuja määritellään näin:

```python
white = (255,255,255)
```

Sitten laitetaan while-silmukan sisään komento, joka täyttää ikkunan värillä. Käytetään tähän
ikkunaa kuvaavaa window-muuttujaa, jonka määrittelimme hetki sitten.

```python
while running:
    # ...

    window.fill(white)
    pygame.display.update()
```

Lisäksi lisäsimme komennon, joka päivittää ikkunan. Muuten mikään piirtämämme ei näy ikkunassa.
Kun lisäät joitakin muita piirtämiskomentoja, niin jätä update-komento viimeiseksi.

### Hahmon piirtäminen

![leppis.png](leppis.png)

Lisätään peliin hahmo. Ensin meidän täytyy ladata hahmon kuva ohjelmaan. Voit käyttää tätä 
leppäkerttuhahmoa tai jotain muuta haluamaasi kuvaa. Laita kuva samaan kansioon kuin 
kooditiedosto ja laita sen nimeksi leppis.png. Laitetaan koodin alkupäähän tämä koodirivi, 
joka lataa kuvan muuttujaan leppis\_image:

```python
leppis_image = pygame.image.load("leppis.png")
```

Sitten meidän täytyy tehdä kaksi muuttujaa, jotka kuvaavat leppiksen sijaintia ikkunassa. Toinen
on x-koordinaatti eli sijainti vaaka-akselilla, ja toinen on y-koordinaatti eli sijainti 
pystyakselilla. Akselit alkavat nollasta, ja x-koordinaatti nolla on ikkunan vasemmassa
reunassa, ja y-koordinaatti nolla on ikkunan yläreunassa. Valitse siis x-koordinaatti 
nollan ja ikkunan leveyden väliltä, ja vastaavasti y-koordinaatti nollan ja ikkunan korkeuden
väliltä.

Määritellään alkuun nämä muuttujat:

```python
leppis_x = 350
leppis_y = 500
```

Ja sitten laitetaan while-silmukkaan komento, joka piirtää leppiksen:

```python
while running:
    # ...
    window.fill(white)
    window.blit(leppis_image, (leppis_x, leppis_y))
    pygame.display.update()
```

Muista laittaa piirtämiskomennot fill-komennon jälkeen. Muuten piirretyt asiat jäävät valkoisen
taustan taakse.

{{% huom %}}

Huom: Blit-piirtämiskomento ottaa ensimmäiseksi argumentiksi piirrettävän kuvan, ja 
toiseksi argumentiksi tuplen, joka sisältää x- ja y-koordinaatit. Muista siis sulut 
koordinaattien ympärille.

{{% /huom %}}

Nyt kun suoritat koodin, niin leppiksen pitäisi näkyä ikkunassa. Koko koodin pitäisi näyttää
tältä:

```python
import pygame

window = pygame.display.set_mode((1000, 800))
pygame.display.set_caption("Leppispeli")

fps = 60
clock = pygame.time.Clock()

white = (255, 255, 255)
leppis_image = pygame.image.load("leppis.png")
leppis_x = 350
leppis_y = 500

running = True

while running:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    window.fill(white)
    window.blit(leppis_image, (leppis_x, leppis_y))
    pygame.display.update()
```

[Seuraava artikkeli: Hahmon liikuttaminen](/pelin_tekeminen/hahmon_liikuttaminen)
