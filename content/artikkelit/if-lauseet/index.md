---
title: If-lauseet
date: 2022-01-03
---

Joskus halutaan, että tietty koodipätkä suoritetaan vain, jos tietty ehto toteutuu. Tämän 
toteuttamiseen käytetään if-lausetta:

```python
if ika < 0:
    print("ikä ei voi olla pienempi kuin 0!")
```

Usein näissä lauseissa vertaillaan kahta numeroa. Tämä onnistuu näillä merkeillä:

- == (yhtä suuri kuin)
- != (ei yhtä suuri kuin)
- \> (suurempi kuin)
- \< (pienempi kuin)
- \>= (suurempi tai yhtä suuri kuin)
- \<= (pienempi tai yhtä suuri kuin)

Jos if-lauseen ehto on tosi, niin sen sisällä oleva koodi suoritetaan. Ehdoksi voi myös antaa muuttujan.

If-lauseelle voi myös antaa monta ehtoa. Ehtoja voi ketjuttaa näillä sanoilla:

- and (ovatko molemmat ehdot tosia)
- or (onko ainakin toinen ehdoista tosi)
- not (ehdon vastakohta)

Näitä käytetään esimerkiksi näin:

```python
if pelaajan_elamat == 0 and not kuolematon_pelaaja:
    lopeta_peli()
```

Jos ehtoja on monta ja niiden ryhmittely saattaa jäädä epäselväksi, niin kannattaa laittaa yhteen 
kuuluvat ehdot sulkujen sisään.

Tässä esimerkissä näkyy, miten ehtojen sarjasta saa vaiheittain ratkaistua totuusarvon:

```
numero1 = 6
numero2 = 100
numero3 = -50
totuus1 = True
totuus2 = False

(((numero1 > 7) or totuus1) or ((numero2 * 3 < 400) and totuus2)) and numero3 == -50
↓
(((6 > 7) or totuus1) or ((300 < 400) and totuus2)) and -50 == -50
↓
((False or True) or (True and False)) and True
↓
(True or False) and True
↓
True and True
↓
True
```

Siis jos tämä ehtojen sarja olisi laitettu if-lauseen ehdoksi, niin ehtolauseen sisäinen koodi olisi
suoritettu, koska ehto oli tosi. If-lauseet siis toimivat näin:

```python
if True:
    print("Tämä tulostetaan")
if False:
    print("Tätä ei tulosteta")
```

### If, elif ja else

Usein halutaan määritellä myös, mitä tapahtuu, jos jokin ehto ei toteudu. Tähän voi käyttää 
if-else-rakennetta:

```python
if numero % 2 == 1:
    print("pariton luku")
else:
    print("parillinen luku")
```

Else-rakennetta ei voi käyttää ilman edeltävää if-rakennetta.

Vaihtoehtoja voi olla myös enemmän kuin 2. Tähän käytetään if-elif-else-rakennetta:

```python
if ehto:
    print("ensimmäinen ehto toteutui")
elif ehto2:
    print("ensimmäinen ehto ei toteutunut, mutta toinen ehto toteutui")
elif ehto3:
    print("ensimmäinen tai toinen ehto ei toteutunut, mutta kolmas ehto toteutui")
else:
    print("mikään ehto ei toteutunut")
```

Elif on lyhenne sanoista else if. Elif korvaa monimutkaisemman rakenteen, jossa vaihtoehtoisia 
if-lauseita tehdään else-rakenteen sisään:

```python
if ehto:
    print("ensimmäinen ehto toteutui")
else:
    if ehto2:
        print("ensimmäinen ehto ei toteutunut, mutta toinen ehto toteutui")
    else:
        if ehto3:
            print("ensimmäinen tai toinen ehto ei toteutunut, mutta kolmas ehto toteutui")
        else:
            print("mikään ehto ei toteutunut")
```

Kun käytetään if-elif-else-rakennetta, täytyy muistaa, että vain yksi näistä lauseista voi 
toteutua. Jos haluat, että useampia suoritetaan peräkkäin, niin käytä peräkkäisiä tavallisia 
if-lauseita. Mutta jos haluat, että jonkun toisen if-lauseen sisältämä koodi suoritetaan vain, 
jos ensimmäistä if-lausetta ei suoriteta, niin tee if-elif(-else)-rakenne.
