---
title: Pelin loppuminen
date: 2022-05-27
previous: elamat_ja_pisteet
---

Nyt elämät vähenevät, kun kivi osuu leppikseen, mutta elämät voivat mennä alle nollan. Kirjoitetaan 
koodi, joka lopettaa pelin, kun elämiä ei ole enää jäljellä.

Määritetään alkuun muuttuja, joka kertoo, onko peli loppunut:

```python
# ...
running = True
game_over = False

while running:
    # ...
```

Lisätään koodipätkä, joka vaihtaa game\_over-muuttujan todeksi, kun elämiä ei ole enää jäljellä.
Tehdään tämä sen jälkeen, kun elämä vähennetään kiven osuessa leppikseen. Eli näin:

```python
elif rock_hitting_leppis(rock_coords):
    lives -= 1
    deleted_rocks.append(rock_coords)
    if lives == 0:
        game_over = True
```

Sitten tehdään silmukan sisään koodi, joka piirtää loppunäytön, jossa lukee kerätyt pisteet. Tehdään 
tämä sen koodin jälkeen, joka käsittelee ikkunan sulkemistapahtuman, muuten ikkunaa ei pysty sulkemaan.
Piirretään ensin vain taustaväri.

```python
while running:
    # ...

    if game_over:
        window.fill((255,180,200))

        pygame.display.update()
        continue
    # ...
```

Tämä koodi täyttää ikkunan värillä ja päivittää ikkunan, jotta muutokset näkyvät. Sen jälkeen 
`continue`-komento saa koodin menemään while-silmukan alkuun. Mitään tämän jälkeistä while-silmukan 
sisällä olevaa koodia ei siis enää suoriteta.

Tehdään loppuruutuun teksti, jossa lukee "peli loppui" isolla fontilla. Määritetään tätä varten koodin
alkuun isompi fontti:

```python
big_font = pygame.font.SysFont(None, 60)
```

Sitten siirrytään takaisin `if game_over:`-lauseen sisälle ja määritellään teksti, jossa lukee "peli
loppui", ja toinen, jossa lukee kerätyty pisteet. Sen jälkeen piirretään nämä ikkunaan.

```python
game_over_text = big_font.render("peli loppui", True, white)      
game_over_score = font.render("pisteet: " + str(score), True, white)

window.blit(game_over_text, (100,300))
window.blit(game_over_score, (100,390))
```

Piirretään vielä leppis sopivaan kohtaan:

```python
window.blit(leppis_image, (400, 290))
```

Nyt pelillä on loppuruutu.

Jos peli tuntuu liian helpolta, sitä voi vaikeuttaa näin:

Lisätään putoamisnopeutta:

```python
gravity = 9
```

Lisätään kivien syntymistodennäköisyyttä:

```python
# make new rocks and coins
    rand_num = random.randint(0,100)
    # vanha if-lause (poista tämä)
    if rand_num < 5:
        rocks.append([random_x(), -50])
    # uusi if-lause
    if rand_num < 8:
        rocks.append([random_x(), -50])
    elif rand_num > 95:
        coins.append([random_x(), -50])
```

Kasvatetaan putoamisnopeutta samalla kun pisteet kasvavat (lisää tämä while-silmukan sisään):

```python
gravity = 9 + score/500
```

Nyt yksinkertainen peli on valmis. Koko koodi näyttää tältä:

```python
import pygame
import random

pygame.font.init()
font = pygame.font.SysFont(None, 30)
fontColor = (234,100,176)

big_font = pygame.font.SysFont(None, 60)

window_width = 1000
window_height = 800
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Leppispeli")

fps = 60
clock = pygame.time.Clock()

white = (255, 255, 255)

ground_color = (171, 237, 130)
ground_start_y = 550
ground = pygame.Rect(0, ground_start_y, window_width, window_height - ground_start_y)

leppis_image = pygame.image.load("leppis.png")
leppis_x = 350
leppis_y = ground_start_y - leppis_image.get_height() + 12
leppis_speed = 7
lives = 10
score = 0

rock_image = pygame.image.load("rock.png")
coin_image = pygame.image.load("coin.png")

# get a random x coordinate for new rocks and coins
def random_x():
    return random.randint(0, window_width - rock_image.get_width())

rocks = [ [random_x(), -50] ]
coins = [ [random_x(), -50] ]

gravity = 9

def rock_hitting_leppis(rockCoords):
    if rockCoords[0] > leppis_x + leppis_image.get_width() - 16:
        return False
    elif rockCoords[0] + rock_image.get_width() < leppis_x:
        return False
    elif rockCoords[1] + rock_image.get_height() < leppis_y:
        return False
    return True

def coin_hitting_leppis(coinCoords):
    if coinCoords[0] > leppis_x + leppis_image.get_width():
        return False
    elif coinCoords[0] + coin_image.get_width() < leppis_x:
        return False
    elif coinCoords[1] + coin_image.get_height() < leppis_y:
        return False
    return True

running = True
game_over = False

# do everything inside this loop 60 times a second
while running:
    # limit fps
    clock.tick(fps)

    # handle window close event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # draw game over screen
    if game_over:
        window.fill((255,180,200))
        game_over_text = big_font.render("peli loppui", True, white)
        game_over_score = font.render("pisteet: " + str(score), True, white)

        window.blit(game_over_text, (100,300))
        window.blit(game_over_score, (100,390))

        window.blit(leppis_image, (400, 290))
        
        pygame.display.update()
        continue

    # handle key presses
    pressed_keys = pygame.key.get_pressed()

    if pressed_keys[pygame.K_LEFT] and leppis_x - leppis_speed > 0:
        leppis_x = leppis_x - leppis_speed
    right_boundary = window_width - leppis_image.get_width()
    if pressed_keys[pygame.K_RIGHT] and leppis_x + leppis_speed < right_boundary:
        leppis_x = leppis_x + leppis_speed
    
    # make new rocks and coins
    rand_num = random.randint(0,100)
    if rand_num < 8:
        rocks.append([random_x(), -50])
    elif rand_num > 95:
        coins.append([random_x(), -50])

    gravity = 9 + score/500

    # move rocks and coins
    deleted_rocks = []
    for rock_coords in rocks:
        rock_coords[1] += gravity 
        if rock_coords[1] > ground_start_y - rock_image.get_height():
            deleted_rocks.append(rock_coords)
        elif rock_hitting_leppis(rock_coords):
            lives -= 1
            deleted_rocks.append(rock_coords)
            if lives == 0:
                game_over = True
    for rock in deleted_rocks:
        rocks.remove(rock)

    deleted_coins = []
    for coin_coords in coins:
        coin_coords[1] += gravity    
        if coin_coords[1] > ground_start_y - coin_image.get_height():
            deleted_coins.append(coin_coords)
        elif coin_hitting_leppis(coin_coords):
            score += 10
            deleted_coins.append(coin_coords)
    for coin in deleted_coins:
        coins.remove(coin)
    
    # draw white background, ground, and leppis
    window.fill(white)
    pygame.draw.rect(window, ground_color, ground)
    window.blit(leppis_image, (leppis_x, leppis_y))
    
    # draw rocks and coins
    for rock_coords in rocks:
        window.blit(rock_image, rock_coords)
    for coin_coords in coins:
        window.blit(coin_image, coin_coords)
    
    # draw lives and score text
    scoreText = font.render("pisteet: " + str(score), True, fontColor)
    livesText = font.render("elämät: " + str(lives), True, fontColor)
    window.blit(scoreText, (20,20))
    window.blit(livesText, (20,60))

    # show the new frame
    pygame.display.update()
```
