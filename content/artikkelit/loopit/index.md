---
title: Loopit
date: 2022-01-05
---

Looppi (eli silmukka) tarkoittaa koodirakennetta, joka suorittaa saman koodin monta kertaa 
peräkkäin. Looppeja on Pythonissa kahdenlaisia.

### While-loop

{{% large-img 
    src="/images/while.png" 
    maxwidth="40rem" 
    class="img-with-border invertable-colors-img" 
%}}

While-loop toimii näin:

```python
while ehto:
    print("looppi")
```

Syntaksi muistuttaa if-lausetta. Siis jos ehto toteutuu, niin loopin sisällä olevaa koodia 
suoritetaan kunnes ehto ei enää toteudu. Ehdon toteutuminen tarkistetaan kierrosten välillä,
eli vaikka ehto muuttuisi epätodeksi kesken kierroksen, niin jäljellä olevat koodirivit 
suoritetaan silti.

Loopin pitäisi pystyä loppumaan. Jos ehto pysyy aina totena, niin "looppi"-riviä tulostetaan 
ikuisesti, eikä koodin suoritus pysty etenemään looppia pitemmälle. 

Yksi tapa määritellä, 
kuinka monta kertaa loopin sisältö suoritetaan, on tehdä muuttuja, joka laskee, kuinka monta
kertaa looppi on suoritettu. Loopin sisälle lisätään koodirivi, joka korottaa tämän muuttujan
arvoa. Useimmiten tällaiset laskurimuuttujat nimetään `i`:ksi, mutta myös muuta nimeä voi 
käyttää. Lisäksi silmukan suoritusehdoksi laitetaan vertailu, joka tarkistaa, onko 
laskurimuuttujan arvo jo tarpeeksi iso. Esimerkiksi "loopin sisältö suoritetaan, jos laskurin
arvo on pienempi kuin 5":

```python
i = 0
while i < 5:
    print("looppi")
    i += 1
print("looppi ohi")
```

```tulostus
looppi
looppi
looppi
looppi
looppi
looppi ohi
```

`i`:n arvoa voi myös käyttää koodissa:

```python
i = 1
while i <= 5:
    print(i * "o")
    i += 1
```

```tulostus
o
oo
ooo
oooo
ooooo
```

Loopin keskeyttämiseen voi käyttää myös `break`-avainsanaa. Break siis keskeyttää loopin 
suorittamisen (missä kohdassa tahansa), ja suoritus jatkuu loopin jälkeisestä koodirivistä. 
Breakkia voi käyttää laskurimuuttujan kanssa näin:

```python
i = 0
while True:
    print(i)
    i += 1
    if i > 5:
        break

print("Looppi ohi")
```

```tulostus
0
1
2
3
4
5
Looppi ohi
```

### For-loop

{{% large-img 
    src="/images/for-loop.png" 
    maxwidth="25rem" 
    class="img-with-border invertable-colors-img" 
%}}

For-looppi käy läpi jonkin kokoelman (esim. listan) kaikki alkiot yksitellen. For-loop on 
helppokäyttöinen, koska sitä käyttäessä ei tarvitse laskurimuuttujaa. Tässä esimerkki
for-loopista:

```python
lista = [1,5,9,13,17]
for numero in lista:
    print(numero)
```

```tulostus
1
5
9
13
17
```

Eli for-sanan jälkeen täytyy kirjoittaa uuden muuttujan nimi, johon listan alkiot sijoitetaan
yksitellen loopissa, ja in-sanan jälkeen laitetaan listamuuttujan nimi. Sitten sisennettyyn 
osaan kirjoitetaan koodi, jossa tehdään jotain näille alkioille. Tässä esimerkissä
jokainen tulostetaan.

Saman asian pystyy tekemään myös while-loopilla:

```python
i = 0
while i < len(lista):
    print(lista[i])
    i += 1
```

For-looppi on siis syntaksiltaan vähän yksinkertaisempi. Kuitenkin jos listan alkioita haluaa 
muokata loopin sisällä, niin siihen tarvitsee alkion indeksin (esim. `lista[i] += 1`). Jos 
esimerkiksi edellisessä for-loopissa muuttaa numero-muuttujan arvoa, niin se ei tee 
alkuperäiselle listan alkiolle mitään, vaan pelkästään numero-muuttujan arvo muuttuu yhden
kierroksen ajaksi.

For-loopilla voi myös käydä läpi numeroita:

```python
for i in range(0,5):
    print(i)
```

```tulostus
0
1
2
3
4
```

Range-funktion avulla määritellään i:n alku- ja loppupiste. Se palauttaa objektin, joka on vähän
niin kuin lista, joka sisältää arvot alkupisteestä loppupisteeseen. Huomaa, että tämän "listan"
eli myös i:n viimeinen arvo on loppupistettä edeltävä luku. Eli tässä tapauksessa loppupiste on 5, ja viimeinen arvo on 4. For-loopissa i:n arvoa ei tarvitse kohottaa itse, vaan for-looppi 
siirtyy automaattisesti seuraavaan arvoon.

Myös for-loopin suorituksen voi lopettaa break-avainsanalla.
