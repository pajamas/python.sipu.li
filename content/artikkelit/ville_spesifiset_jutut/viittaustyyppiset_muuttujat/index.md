---
title: Viittaustyyppiset muuttujat
---

{{% huom %}}
Huom! Tämä artikkeli on kategoriassa 'ViLLE-spesifiset jutut' sen takia, että se
käyttää ViLLEä esimerkkinä. Viittaustyyppiset muuttujat toimivat samalla tavalla
myös ViLLEn ulkopuolella.
{{% /huom %}}

Olet tekemässä ViLLE-tehtävää, jossa pitäisi tehdä funktio, 
joka tyhjentää parametrina annetun listan.

ViLLEn testikoodissa luodaan lista, ja annetaan se parametriksi sinun funktiollesi:

```python
villen_lista = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print("Lista ennen:", villen_lista)
tyhjenna(villen_lista)
print("Lista jälkeen:", villen_lista)
```

Lähdetään koodaamaan funktiota. Yrität toteuttaa funktion näin:

```python
def tyhjenna(lista):
    lista = []
```

Mutta hups! ViLLE sanookin näin:

```
Lista ennen: [1, 2, 3, 4, 5, 6, 7, 8, 9]
Lista jälkeen: [1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Miksei lista muuttunut??

`tyhjenna`-funktion sisäinen muuttuja `lista` on viittaustyyppinen muuttuja. 
Tämä tarkoittaa, että se ei sisällä listaa, vaan listan osoitteen muistissa:

{{% large-img src="listaviittaus.png" maxwidth="65rem" 
    class="invertable-colors-img img-with-border" %}}

Myös ViLLEn testikoodin `villen_lista`-muuttuja osoittaa samaan listaan muistissa
(`villen_lista`han oli se alkuperäinen listamuuttuja, jonka viittaus annettiin 
funktiokutsun yhteydessä `lista`-muuttujalle):

{{% large-img src="viittaustyyppiset.png" maxwidth="65rem" 
    class="invertable-colors-img img-with-border" %}}

Mutta mitä tapahtuu `tyhjenna`-funktiossa?

Rivillä `lista = []` luodaan uusi tyhjä lista, jonka viittaus tallennetaan muuttujaan
`lista`. `lista` on `tyhjenna`-funktion sisäinen muuttuja, joten siihen tehdyt muutokset
eivät näy mitenkään funktion ulkopuolella. Tulos on tämä:

{{% large-img src="viittaustyyppiset2.png" maxwidth="65rem" 
    class="invertable-colors-img img-with-border" %}}

Alkuperäistä listaa ei siis ole tyhjennetty. Mutta miten se sitten tyhjennetään?

Sinun täytyy välttää minkään koodirivin kirjoittamista, jossa on alussa "`lista =`",
sillä tällaisilla riveillä tallennetaan lista-muuttujaan jotain muuta kuin sen 
alkuperäinen arvo (viittaus alkuperäiseen listaan). Sitten sinulla ei ole enää
mitään viittausta alkuperäiseen listaan, jonka kautta voisit sitä muokata.

Pythonissa on monia funktioita, jotka muokkaavat alkuperäistä listaa
uuden luomisen sijaan. Tässä joitain esimerkkejä niistä:

- remove
- append
- clear
- pop
- sort
- reverse

Näistä tähän tehtävään toimivin on funktio `clear`, joka poistaa listasta kaikki alkiot
luomatta uutta listaa:

{{% large-img src="viittaustyyppiset3.png" maxwidth="65rem"
    class="invertable-colors-img img-with-border" %}}

Korjattu koodi näyttää tältä:

```python
def tyhjenna(lista):
    lista.clear()
```
