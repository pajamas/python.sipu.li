---
title: Merkkijonot ja syöte
date: 2022-01-02
---

Merkkijono tarkoittaa tekstiä. Pythonissa merkkijono muodostetaan lainausmerkeillä: `"tämä on 
merkkijono"`. Myös heittomerkkejä voi käyttää: `'tämäkin on merkkijono'`, mutta on parempi 
käyttää lainausmerkkejä, sillä joissain muissa ohjelmointikielissä heittomerkkejä ei voi 
käyttää merkkijonoon.

### Merkkijonojen yhdistäminen

Merkkijonoja voi yhdistää `+`-merkillä:

```python
jono1 = "Hello"
jono2 = "World!"
print(jono1 + " " + jono2)
```

```tulostus
Hello World!
```

Merkkijonoja voi muokata myös kertomalla:

```python
jono = "abc"
pitkajono = jono*5
print(pitkajono)
```

```tulostus
abcabcabcabcabc
```

### Muiden datatyyppien yhdistäminen merkkijonoon

Jos haluat tulostaa tekstiä, joka sisältää muuttujan arvon, voit tehdä sen näin:

```python
numero = 14
merkkijono = "numero: "
print(merkkijono + str(numero))
```

```tulostus
numero: 14
```

Merkkijonoa ei siis voi yhdistää suoraan toiseen datatyyppiin, vaan toinen datatyyppi 
täytyy ensin muuntaa merkkijonoksi `str`-funktiolla.

Tässä toinen tapa sisällyttää muuttujien arvoja merkkijonoihin:

```python
num = 5
print(f"Numero on {num}.")
```

```tulostus
Numero on 5.
```

Merkkijonoja, joihin on tällä tavalla sisällytetty muuttujan arvo, sanotaan usein f-stringeiksi
(string on merkkijono englanniksi). F-kirjain ennen merkkijonoa tarkoittaa siis sitä, että 
merkkijonon sisäisiä aaltosulkeita ei käsitellä tavallisina merkkeinä, vaan niiden sisällä on
koodia (esimerkiksi muuttujan nimi), joka muunnetaan osaksi merkkijonoa.

### Käyttäjän syöte

Jos haluat pyytää käyttäjää kirjoittamaan jonkun syötteen, jotta voit käyttää sitä koodissasi,
tämä onnistuu `input`-komennolla.

```python
syote = input("Anna jokin syöte: ")
print(syote)
```

Kun tämän koodin ajaa, niin käyttäjältä kysytään syötettä. Käyttäjä voi kirjoittaa jotain ja
painaa enteriä, ja sitten tämä syöte tallentuu muuttujaan syote, joka tulostetaan seuraavalla
rivillä.

Käyttäjän syöttämä teksti tallentuu aina merkkijonona, vaikka syöte koostuisi numeroista. Jos
haluat muuntaa merkkijonon numeroksi, käytä joko `int`- tai `float`-funktiota (int muuntaa
merkkijonon kokonaisluvuksi ja float desimaaliluvuksi):

```python
n1 = input("Anna numero: ")
n2 = input("Anna toinen numero: ")
summa = int(n1) + int(n2)
print(f"Lukujen summa on {summa}.")
```

### Tehtävät

{{% exercise num="1" %}}
Yhdistä kaksi merkkijonoa plussaamalla ja tulosta lopputulos.
{{< solution >}}
```python
yhdistelma = "abc" + "def"
print(yhdistelma)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="2" %}}
Yhdistä kaksi merkkijonoa plussaamalla. Tulosta lopputulos kerrottuna kolmella.
{{< solution >}}
```python
yhdistelma = "abc" + "def"
print(yhdistelma*3)
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="3" %}}
Tallenna muuttujaan jokin arvo, joka ei ole merkkijono (esimerkiksi numero). Tulosta tämän 
muuttujan arvo osana jotakin merkkijonoa.
{{< solution >}}
```python
paidanHinta = 20
# vaihtoehto 1
print("Paita maksaa " + str(paidanHinta) + " euroa.")
# vaihtoehto 2
print(f"Paita maksaa {paidanHinta} euroa.")
```
{{< /solution >}}
{{% /exercise %}}

{{% exercise num="4" %}}
Kysy käyttäjältä ikää ja tallenna se muuttujaan. Tulosta ikä osana merkkijonoa.
{{< solution >}}
```python
ika = input("Minkä ikäinen olet? ")
print(f"Olet {ika} vuotta vanha.")
```
{{< /solution >}}
