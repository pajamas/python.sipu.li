---
title: Virheviestien tulkitseminen
date: 2022-01-07
---

### SyntaxError

SyntaxError tarkoittaa, että olet kirjoittanut koodipätkän jotenkin väärin, eikä koodi
vastaa Pythonin "kielioppia". Tässä pari esimerkkiä niistä.

#### Esimerkki 1

```python
def funktio:
    print("tässä on virhe")
```

```tulostus
  File "/home/anna/python/virhe.py", line 1
    def funktio:
               ^
SyntaxError: invalid syntax
```

Koodissa on yritetty määritellä funktiota, mutta funktion sulut puuttuvat. Korjattu koodi:

```python
def funktio():
    print("virhe korjattu")
```

#### Esimerkki 2

```python
def funktio():
    print "tässäkin on virhe"
```

```tulostus
  File "/home/anna/python/virhe.py", line 2
    print "tässäkin on virhe"
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(...)?
```

Tässä koodissa on kutsuttu print-funktiota ilman sulkuja. Korjattu koodi:

```python
def funktio():
    print("virhe korjattu")
```

### NameError

NameError tarkoittaa sitä, että olet käyttänyt jotain muuttujaa tai funktiota, jota ei ole
olemassa.

#### Esimerkki 1

```python
numero = 10
print(numeero)
```

```tulostus
Traceback (most recent call last):
  File "/home/anna/python/virhe.py", line 2, in <module>
    print(numeero)
NameError: name 'numeero' is not defined. Did you mean: 'numero'?
```

Koodissa on kirjoitettu muuttujan nimi väärin. Korjattu koodi:

```python
numero = 10
print(numero)
```

#### Esimerkki 2

```python
def funktio():
    luku = input("Anna luku: ")
    print(f"Luku kerrottuna kahdella on {luku*2}.")

print(f"Luku kerrottuna kolmella on {luku*3}.")
```

```tulostus
Traceback (most recent call last):
  File "/home/anna/python/virhe.py", line 5, in <module>
    print(f"Luku kerrottuna kolmella on {luku*3}.")
NameError: name 'luku' is not defined
```

Tässä koodissa on määritelty funktion sisällä muuttuja luku. Muuttujaa ei kuitenkaan voi käyttää
funktion ulkopuolella. Joko muuttuja täytyy määritellä funktion ulkopuolella, tai sitä käyttävä
rivi täytyy siirtää funktion sisään. Korjattu koodi:

```python
def funktio():
    luku = input("Anna luku: ")
    print(f"Luku kerrottuna kahdella on {luku*2}.")
    print(f"Luku kerrottuna kolmella on {luku*3}.")
```
